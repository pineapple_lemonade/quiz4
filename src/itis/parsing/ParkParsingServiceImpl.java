package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        ArrayList<String> list = new ArrayList<>();
        try {
            File file = new File(parkDatafilePath);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            list = (ArrayList<String>) bufferedReader.lines().collect(Collectors.toList());
            bufferedReader.close();
            list = (ArrayList<String>) list.stream()
                    .filter((o) -> !o.contains("***"))
                    .map((o) -> o.replaceAll("\"", ""))
                    .map((o) -> o.replaceAll(" ", ""))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Class<Park> parkClass = Park.class;
        ArrayList<Field> fields = (ArrayList<Field>) Arrays.stream(parkClass.getDeclaredFields())
                .peek(field -> field.setAccessible(true))
                .collect(Collectors.toList());
        Constructor<Park> declaredConstructor = parkClass.getDeclaredConstructor();
        declaredConstructor.setAccessible(true);
        Park park = declaredConstructor.newInstance();
        for (int i = 0; i < fields.size(); i++) {
            Annotation[] annotations1 = fields.get(i).getAnnotations();
            if (Arrays.stream(annotations1).anyMatch(annotation -> annotation instanceof FieldName)) {
                FieldName fieldName = null;
                for (int j = 0; j < annotations1.length; j++) {
                    if (annotations1[i] instanceof FieldName) {
                        fieldName = (FieldName) annotations1[i];
                    }
                }
                String strToSet = null;
                for (int j = 0; j < list.size(); j++) {
                    if (list.get(j).contains(fieldName.value())) {
                        String[] strings = list.get(j).split(":");
                        if (!strings[1].equals("")) {
                            strToSet = strings[1];
                        }
                    }
                }
                if(fields.get(i).getName() == "foundationYear"){
                    fields.get(i).set(park,LocalDate.parse(strToSet));
                } else {
                    fields.get(i).set(park, strToSet);
                }
            } else{
                String strToSet = null;
                for (int j = 0; j < list.size(); j++) {
                    if (list.get(j).contains(fields.get(i).getName())) {
                        String[] strings = list.get(j).split(":");
                        if (!strings[1].equals("")) {
                            strToSet = strings[1];
                        }
                    }
                }
                if(fields.get(i).getName() == "foundationYear"){
                    fields.get(i).set(park,LocalDate.parse(strToSet));
                } else {
                    fields.get(i).set(park, strToSet);
                }
            }
            if (Arrays.stream(annotations1).anyMatch(annotation -> annotation instanceof MaxLength)) {
                MaxLength maxLength = null;
                for (int j = 0; j < annotations1.length; j++) {
                    if (annotations1[i] instanceof MaxLength) {
                        maxLength = (MaxLength) annotations1[i];
                    }
                }
                for (int j = 0; j < list.size(); j++) {
                    if (list.get(j).contains(fields.get(i).getName())) {
                        String[] strings = list.get(j).split(":");
                        if (!strings[1].equals("") && strings[1].length() <= maxLength.value()) {
                            fields.get(i).set(park,null);
                        }
                    }
                }
            }
            if (Arrays.stream(annotations1).anyMatch(annotation -> annotation instanceof NotBlank)) {

            }
        }
        return park;
    }
}

