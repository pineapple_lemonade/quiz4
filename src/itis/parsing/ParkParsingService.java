package itis.parsing;

import java.lang.reflect.InvocationTargetException;

interface ParkParsingService {

    Park parseParkData(String parkDatafilePath) throws ParkParsingException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException;

}
